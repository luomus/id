package fi.luomus.id;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.id.model.ResourceInfo;

@WebServlet(urlPatterns = {"/class/*"})
public class ClassResolveService extends IdBaseServlet {

	private static final long serialVersionUID = -1018689842142256951L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String[] uris = req.getParameterValues("uri");
		String[] qnames = req.getParameterValues("qname");

		if (uris == null && qnames == null) return status(400, res);

		List<Pair<String, Qname>> parameters = getParameters(uris, qnames);

		if (parameters.isEmpty()) return status(400, res);

		JSONObject response = new JSONObject();
		for (Pair<String, Qname> parameter : parameters) {
			if (!isValidURI(parameter.getValue())) continue;
			JSONObject json = response.getObject(parameter.getKey());
			Optional<ResourceInfo> info = getDao().resolve(parameter.getValue());
			if (info.isPresent()) {
				json.setString("class", info.get().getType().toString());
				if (info.get().getParentId().isPresent()) {
					json.setString("parent", info.get().getParentId().get().toString());
					json.setString("parentUri", info.get().getParentId().get().toURI());
				}
			}
		}
		return jsonResponse(response);
	}

	private List<Pair<String, Qname>> getParameters(String[] uris, String[] qnames) {
		List<Pair<String, Qname>> parameters = new ArrayList<>();
		parseUris(uris, parameters);
		parseQnames(qnames, parameters);
		return parameters;
	}

	private void parseQnames(String[] qnames, List<Pair<String, Qname>> parameters) {
		if (qnames == null) return;
		for (String qname : qnames) {
			try {
				qname = qname.trim();
				if (given(qname)) parameters.add(new Pair<>(qname, new Qname(qname)));
			} catch (Exception e) {}
		}
	}

	private void parseUris(String[] uris, List<Pair<String, Qname>> parameters) {
		if (uris == null) return;
		for (String uri : uris) {
			try {
				uri = uri.trim();
				if (given(uri)) parameters.add(new Pair<>(uri, Qname.fromURI(uri)));
			} catch (Exception e) {}
		}
	}

}
