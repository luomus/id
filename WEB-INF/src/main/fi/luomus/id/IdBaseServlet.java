package fi.luomus.id;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.BaseServlet;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.dao.DAOImple;
import fi.luomus.id.dao.OracleDataSourceDefinition;
import fi.luomus.id.resolvers.ResolveService;
import fi.luomus.id.resolvers.Resolvers;
import fi.luomus.id.viewers.ViewerMappings;

public abstract class IdBaseServlet extends BaseServlet {

	private static final long serialVersionUID = 4611886483221777228L;

	private static DAOImple dao;
	private static ViewerMappings viewers;
	private static DataSource oracleDataSource;
	private static Resolvers resolvers;

	@Override
	protected void applicationInitOnlyOnce() {
		oracleDataSource = OracleDataSourceDefinition.initDataSource(getConfig().connectionDescription());
		resolvers = new Resolvers(getConfig(), oracleDataSource);
		dao = new DAOImple(oracleDataSource, getConfig(), getErrorReporter(), resolvers.getServices());
		viewers = new ViewerMappings(dao, getErrorReporter());
	}

	@Override
	protected void applicationDestroy() {
		closeDao();
		closeDataSource();
		closeResolvers();
	}

	private void closeResolvers() {
		if (resolvers != null) {
			try {
				for (ResolveService r : resolvers.getServices()) {
					closeResolver(r);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void closeResolver(ResolveService r) {
		try {
			r.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeDataSource() {
		if (oracleDataSource != null) {
			try {
				oracleDataSource.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void closeDao() {
		if (dao != null) dao.close();
	}

	protected DAO getDao() {
		return dao;
	}

	protected ViewerMappings getViewerMappings() {
		return viewers;
	}

	@Override
	protected void applicationInit() {}

	@Override
	protected String configFileName() {
		return "id-redirect-service.properties";
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	protected boolean isValidURI(Qname id) {
		try {
			new java.net.URI(id.toURI());
			return true;
		} catch (URISyntaxException e) {
			return false;
		}
	}

}
