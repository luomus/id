package fi.luomus.id;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.Mapping.Pair;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfResource;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;
import fi.luomus.id.viewers.ViewerMapping;
import fi.luomus.id.viewers.ViewerMapping.Format;
import fi.luomus.id.viewers.ViewerMapping.UnsupportedFormatException;

@WebServlet(urlPatterns = {"/*"})
public class IdService extends IdBaseServlet {

	private static final long serialVersionUID = -1018689842142256951L;

	private static final List<String> LOCALE_PARAMETERS = Utils.list("lang", "locale", "language");

	private static final List<String> ACCEPT_HEADER_VARIATIONS_LEGACY = Utils.list("accept", "Accept", "accepts", "Accepts");

	public static final Map<String, Format> ACCEPT_HEADER_TO_FORMAT;
	public static final Map<Format, String> FORMAT_TO_ACCEPT_HEADER;
	static {
		ACCEPT_HEADER_TO_FORMAT = new HashMap<>();
		ACCEPT_HEADER_TO_FORMAT.put("application/skos+rdf+xml", Format.SKOS);
		ACCEPT_HEADER_TO_FORMAT.put("application/rdf+xml", Format.RDFXML);
		ACCEPT_HEADER_TO_FORMAT.put("application/xml", Format.XML);
		ACCEPT_HEADER_TO_FORMAT.put("application/json", Format.JSON);
		ACCEPT_HEADER_TO_FORMAT.put("application/ld+json", Format.JSONLD);
		ACCEPT_HEADER_TO_FORMAT.put("application/trig", Format.TRIG);
		ACCEPT_HEADER_TO_FORMAT.put("text/html", Format.HTML);

		FORMAT_TO_ACCEPT_HEADER = new HashMap<>();
		ACCEPT_HEADER_TO_FORMAT.entrySet().forEach(e->{ FORMAT_TO_ACCEPT_HEADER.put(e.getValue(), e.getKey()); });
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Qname id = resolveId(req);
		if (!given(id) || !isValidURI(id)) {
			res.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return status(400, res);
		}
		Optional<ResourceInfo> info = getDao().resolve(id);
		if (!info.isPresent()) {
			res.sendError(HttpServletResponse.SC_NOT_FOUND);
			return status404(res);
		}

		Map<String, String> localeParameters = getLocaleParameters(req);

		Format format = null;
		try {
			format = getFormatParameter(req);
		} catch (UnsupportedFormatException e) {
			ViewerMapping viewer = getViewerMappings().forType(info.get().getType());
			res.getWriter().write(viewer.getSupportedFormats().toString());
			return status(406, res);
		}

		if (format != null) {
			return view(info.get(), format, localeParameters, res);
		}

		return redirect(info.get(), req, localeParameters, res);
	}

	private ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters, HttpServletResponse res) throws Exception {
		ViewerMapping viewer = getViewerMappings().forType(info.getType());
		try {
			ResponseData responseData = viewer.view(info, format, localeParameters);
			return responseData;
		} catch (UnsupportedFormatException e) {
			res.getWriter().write(viewer.getSupportedFormats().toString());
			return status(406, res);
		}
	}

	private ResponseData redirect(ResourceInfo info, HttpServletRequest req, Map<String, String> localeParameters, HttpServletResponse res) throws Exception {
		ViewerMapping viewer = getViewerMappings().forType(info.getType());
		Format format = getAcceptHeader(req, viewer.getSupportedFormats());
		if (format == null) {
			res.getWriter().write(viewer.getSupportedFormats().stream().map(f->FORMAT_TO_ACCEPT_HEADER.get(f)).collect(Collectors.toList()).toString());
			return status(406, res);
		}
		URI uri = viewer.getRedirectURI(info, format, localeParameters);
		return redirectTo(uri.toString(), 303);
	}

	private Qname resolveId(HttpServletRequest req) {
		String id = getQname(req);
		if (!given(id)) return null;
		return new Qname(addQnamePrefix(req, id));
	}

	private String getQname(HttpServletRequest req) {
		String id = req.getPathInfo();
		if (id == null || id.equals("/")) {
			id = "";
		}
		id = id.replaceFirst("/", "");
		if (id.contains(",")) {
			id = id.substring(0, id.indexOf(','));
		}
		return id;
	}

	private String addQnamePrefix(HttpServletRequest req, String qname) {
		String requestURI = req.getRequestURL().toString();
		if (requestURI.startsWith("https:")) requestURI = requestURI.replace("https:", "http:");
		if (requestURI.startsWith(RdfResource.DEFAULT_NAMESPACE_URI)) return qname;
		for (Pair<String> p : RdfResource.NAMESPACES) {
			String namespaceURI = p.getValue2();
			if (requestURI.startsWith(namespaceURI)) {
				String qnamePrefix = p.getValue1();
				return qnamePrefix + ":" + qname;
			}
		}
		return qname;
	}

	private Map<String, String> getLocaleParameters(HttpServletRequest req) {
		Map<String, String> params = new HashMap<>();
		for (String param : LOCALE_PARAMETERS) {
			String[] paramValues = req.getParameterValues(param);
			if (paramValues == null) continue;
			for (String value : paramValues) {
				if (given(value)) {
					params.put(param, value);
				}
			}
		}
		return params;
	}

	private Format getFormatParameter(HttpServletRequest req) throws UnsupportedFormatException {
		String format = req.getParameter("format");
		if (!given(format)) return null;
		format = format.trim();
		for (Format f : Format.values()) {
			if (f.name().equalsIgnoreCase(format)) {
				return f;
			}
		}
		throw new UnsupportedFormatException(format);
	}

	private Format getAcceptHeader(HttpServletRequest req, List<Format> supportedFormats) {
		String header = getAccepts(req);
		if (header == null) return Format.HTML;
		Format format = acceptHeaderToFormat(header, supportedFormats);
		return format;
	}

	private Format acceptHeaderToFormat(String accept, List<Format> supportedFormats) {
		if (accept == null) return Format.HTML;
		accept = accept.trim().toLowerCase();
		for (String headerPart : accept.split(Pattern.quote(","))) {
			if (headerPart.contains(";")) {
				headerPart = headerPart.split(Pattern.quote(";"))[0];
			}
			headerPart = headerPart.trim().replace(" ", "+");
			Format format = ACCEPT_HEADER_TO_FORMAT.get(headerPart);
			if (format != null && supportedFormats.contains(format)) return format;
			if (headerPart.equals("*/*")) return Format.HTML;
		}
		return null;
	}

	private String getAccepts(HttpServletRequest req) {
		for (String header : ACCEPT_HEADER_VARIATIONS_LEGACY) {
			String value = req.getHeader(header);
			if (given(value)) return value;
		}
		return null;
	}

	private boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

}
