package fi.luomus.id.dao;

import java.util.Map;
import java.util.Optional;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.id.model.ResourceInfo;

public interface DAO {

	public Optional<ResourceInfo> resolve(Qname id);

	public Model getTriplestoreModel(Qname id) throws Exception;

	public JSONObject getLajistoreModel(Qname id) throws Exception;
	
	public String getApiResponse(String endpoint, Qname id, Map<String, String> localeParameters) throws Exception;
	
	public String getApiResponse(String endpoint, Qname id, Map<String, String> localeParameters, Map<String, String> additionalParameters) throws Exception;

	public String getDwResponse(String endpoint, Qname documentId, String contentType) throws Exception;

}
