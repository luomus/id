package fi.luomus.id.dao;

import java.net.URISyntaxException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.http.client.methods.HttpGet;
import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;
import fi.luomus.id.resolvers.ResolveService;

public class DAOImple implements DAO {

	private static final int PREPOPULATE_CACHE_COUNT = 150000;

	private final Config config;
	private final ErrorReporter errorReporter;
	private final List<ResolveService> resolveServices;
	private final Cached<Qname, Optional<ResourceInfo>> cache;
	private final DataSource oracleDataSource;
	private final RequestCountStorer requestCountStorer;
	private final HttpClientService triplestoreClient;
	private final HttpClientService apiClient;
	private final String lajistoreShema;
	private final Object LOCK = new Object();

	private class ResolveCacheLoader implements Cached.CacheLoader<Qname, Optional<ResourceInfo>> {
		@Override
		public Optional<ResourceInfo> load(Qname id) {
			return resolveNonCached(id);
		}
	}

	private class RequestCountStorer implements Runnable {

		private volatile boolean stop = false;

		public void stop() {
			this.stop = true;
		}
		@Override
		public void run() {
			System.out.println(RequestCountStorer.class.getName() + " is running");
			while (!stop) {
				storeRequestCounts();
				try {
					Thread.sleep(1000*60);
				} catch (InterruptedException e) {}
			}
			System.out.println(RequestCountStorer.class.getName() + " stopped!");
		}
	}

	public DAOImple(DataSource oracleDataSource, Config config, ErrorReporter errorReporter, List<ResolveService> resolveServices) {
		System.out.println(DAOImple.class.getName() + " created!");
		this.config = config;
		this.errorReporter = errorReporter;
		this.resolveServices = resolveServices;
		this.apiClient = new HttpClientService();
		this.triplestoreClient = openTriplestoreClient();
		this.cache =  new Cached<>(new ResolveCacheLoader(), 5, TimeUnit.DAYS, PREPOPULATE_CACHE_COUNT);
		this.oracleDataSource = oracleDataSource;
		this.requestCountStorer = new RequestCountStorer();
		this.lajistoreShema = config.get("LajistoreSchema");
		new Thread(requestCountStorer).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				prepopulateCache();
			}
		}).start();
	}

	private HttpClientService openTriplestoreClient() {
		try {
			return new HttpClientService(config.get("TriplestoreURL"), config.get("TriplestoreUsername"), config.get("TriplestorePassword"));
		} catch (Exception e) {
			throw new RuntimeException("Triplestore http client parameters?", e);
		}
	}

	public void close() {
		if (requestCountStorer != null) {
			try {
				requestCountStorer.stop();
				requestCountStorer.notify();
			} catch (Exception e) {}
		}
		try {
			storeRequestCounts();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try { if (triplestoreClient != null) triplestoreClient.close(); } catch (Exception e) {}
		try { if (apiClient != null) apiClient.close(); } catch (Exception e) {}
		System.out.println(DAOImple.class.getName() + " closed!");
	}

	@Override
	public Optional<ResourceInfo> resolve(Qname id) {
		if (id.toString().endsWith("robots.txt")) return Optional.empty();
		if (id.toString().endsWith("favicon.icon")) return Optional.empty();
		if (id.toString().endsWith(".php")) return Optional.empty();
		logRequest(id);
		Optional<ResourceInfo> info = cache.get(id);
		if (!info.isPresent()) cache.invalidate(id);
		return info;
	}

	private Optional<ResourceInfo> resolveNonCached(Qname id) {
		Optional<ResourceInfo> info = getStored(id);
		if (info.isPresent()) return info;

		info = resolveUsingServices(id);
		if (info.isPresent()) {
			storeAsync(info.get());
			return info;
		}
		return Optional.empty();
	}

	private void storeAsync(ResourceInfo info) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				store(info);
			}
		}).start();
	}

	private Optional<ResourceInfo> resolveUsingServices(Qname id) {
		for (ResolveService service : getServicesInOptimizedOrder(id)) {
			try {
				Optional<ResourceInfo> info = service.resolve(id);
				if (info.isPresent()) return info;
			} catch (Exception e) {
				errorReporter.report("Resolving " + id + " using " + service.getClass().getName(), e);
			}
		}
		return Optional.empty();
	}

	private List<ResolveService> getServicesInOptimizedOrder(Qname id) {
		List<ResolveService> services = new ArrayList<>();
		ResolveService primary = getPrimaryServiceBasedOnId(id);
		if (primary != null) services.add(primary);
		for (ResolveService service : this.resolveServices) {
			if (service != primary) {
				services.add(service);
			}
		}
		return services;
	}

	private ResolveService getPrimaryServiceBasedOnId(Qname id) {
		String prefix = getPrefix(id);
		if (prefix == null) return null;
		for (ResolveService service : this.resolveServices) {
			if (service.isOptimizedForQnamePrefix(prefix)) {
				return service;
			}
		}
		return null;
	}

	private String getPrefix(Qname id) {
		if (id.toString().contains(":")) return null;
		if (!id.toString().contains(".")) return null;
		return id.toString().split(Pattern.quote("."))[0];
	}

	private TransactionConnection getOracleConnection() throws SQLException {
		return new SimpleTransactionConnection(oracleDataSource.getConnection());
	}

	private final Map<Qname, Integer> requestCountCache = new HashMap<>();

	private void logRequest(Qname id) {
		synchronized (LOCK) {
			Integer count = requestCountCache.get(id);
			if (count == null) {
				requestCountCache.put(id, 1);
			} else {
				requestCountCache.put(id, count + 1);
			}
		}
	}

	private void storeRequestCounts() {
		Map<Qname, Integer> copy = new HashMap<>();
		synchronized (LOCK) {
			copy.putAll(requestCountCache);
			requestCountCache.clear();
		}
		if (copy.isEmpty()) return;
		for (Map.Entry<Qname, Integer> e : copy.entrySet()) {
			persistRequestCount(e.getKey(), e.getValue());
		}
	}

	private void persistRequestCount(Qname id, int count) {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getOracleConnection();
			con.startTransaction();
			p = con.prepareStatement(" UPDATE resolved SET req_count = req_count + ? WHERE uri = ? ");
			p.setInt(1, count);
			p.setString(2, id.toURI());
			p.executeUpdate();
			con.commitTransaction();
		} catch (Exception e) {
			errorReporter.report("Storing request count " + id, e);
		} finally {
			Utils.close(p, con);
		}
	}

	private void store(ResourceInfo info) {
		TransactionConnection con = null;
		PreparedStatement p = null;
		try {
			con = getOracleConnection();
			con.startTransaction();
			p = con.prepareStatement(" INSERT INTO resolved (uri, type, parent_resource_uri) VALUES (?,?,?) ");
			p.setString(1, info.getId().toURI());
			p.setString(2, info.getType().toString());
			p.setString(3, info.getParentId().isPresent() ? info.getParentId().get().toURI() : null);
			p.executeUpdate();
			con.commitTransaction();
		} catch (Exception e) {
			errorReporter.report("Storing request " + info.getId(), e);
		} finally {
			Utils.close(p, con);
		}
	}

	private Optional<ResourceInfo> getStored(Qname id) {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getOracleConnection();
			p = con.prepareStatement(" SELECT type, parent_resource_uri FROM resolved WHERE uri = ? ");
			p.setString(1, id.toURI());
			rs = p.executeQuery();
			if (!rs.next()) return Optional.empty();
			Qname type = new Qname(rs.getString(1));
			String parentURI = rs.getString(2);
			Qname parentId = parentURI == null ? null : Qname.fromURI(parentURI);
			return Optional.of(new ResourceInfo(id, type, parentId));
		} catch (Exception e) {
			errorReporter.report("Fetching stored " + id, e);
			return Optional.empty();
		} finally {
			Utils.close(p, rs, con);
		}
	}

	private void prepopulateCache() {
		System.out.println("Prepopulating cache with " + PREPOPULATE_CACHE_COUNT + " most requested resource infos");
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		int i = 0;
		try {
			con = getOracleConnection();
			p = con.prepareStatement(" SELECT uri, type, parent_resource_uri FROM resolved ORDER BY req_count DESC FETCH FIRST " + PREPOPULATE_CACHE_COUNT + " ROWS ONLY ");
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				Qname id = Qname.fromURI(rs.getString(1));
				Qname type = new Qname(rs.getString(2));
				String parentURI = rs.getString(3);
				Qname parentId = parentURI == null ? null : Qname.fromURI(parentURI);
				cache.put(id, Optional.of(new ResourceInfo(id, type, parentId)));
				i++;
			}
		} catch (Exception e) {
			errorReporter.report("Prepopulating cache", e);
		} finally {
			Utils.close(p, rs, con);
		}
		System.out.println("Prepopulating cache done! (" + i + ")");
	}

	@Override
	public JSONObject getLajistoreModel(Qname id) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = getOracleConnection();
			p = con.prepareStatement(" select data from "+lajistoreShema+".laji_document where id = ? ");
			p.setString(1, id.toString());
			rs = p.executeQuery();
			if (!rs.next()) return null;
			return new JSONObject(rs.getString(1));
		} finally {
			Utils.close(p, rs, con);
		}
	}

	@Override
	public Model getTriplestoreModel(Qname id) throws Exception {
		String uri = config.get("TriplestoreURL") + "/" + id.toString();
		String rdf = triplestoreClient.contentAsString(new HttpGet(uri));
		return Model.fromRdf(rdf);
	}

	@Override
	public String getApiResponse(String endpoint, Qname id, Map<String, String> localeParameters) throws Exception {
		return getApiResponse(endpoint, id, localeParameters, Collections.emptyMap());
	}

	@Override
	public String getApiResponse(String endpoint, Qname id, Map<String, String> localeParameters, Map<String, String> additionalParameters) throws Exception {
		HttpGet get = buildRequest(id, endpoint, localeParameters, additionalParameters);
		return apiClient.contentAsString(get);
	}

	private HttpGet buildRequest(Qname id, String endpoint, Map<String, String> localeParameters, Map<String, String> additionalParameters) throws URISyntaxException {
		URIBuilder requestUri = new URIBuilder(config.get("LajiApiURL")+endpoint+"/"+id.toString());
		if (!localeParameters.isEmpty()) {
			requestUri.addParameter("lang", localeParameters.values().iterator().next());
		}
		for (Map.Entry<String, String> e : additionalParameters.entrySet()) {
			requestUri.addParameter(e.getKey(), e.getValue());
		}
		requestUri.addParameter("access_token", config.get("LajiApiKey"));
		return new HttpGet(requestUri.getURI());
	}

	@Override
	public String getDwResponse(String endpoint, Qname documentId, String contentType) throws Exception {
		URIBuilder requestUri =
				new URIBuilder(config.get("LajiApiURL")+"warehouse/query/"+endpoint+"/")
				.addParameter("documentId", documentId.toURI())
				.addParameter("access_token", config.get("LajiApiKey"));
		HttpGet get = new HttpGet(requestUri.getURI());
		get.setHeader("Accept", contentType);
		String response = apiClient.contentAsString(get);
		return response;
	}

}
