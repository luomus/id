package fi.luomus.id.dao;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class OracleDataSourceDefinition {

	public static DataSource initDataSource(ConnectionDescription desc) {
		PoolProperties p = new PoolProperties();
		p.setUrl(desc.url());
		p.setDriverClassName(desc.driver());
		p.setUsername(desc.username());
		p.setPassword(desc.password());

		p.setDefaultAutoCommit(true); // non-transaction mode: connection will be set to transaction mode if updates are done
		p.setMaxActive(5);
		p.setMaxWait(15 * 1000); // 15 sec to wait for free connection before failing
		p.setMaxIdle(2);
		p.setMinIdle(1);
		p.setInitialSize(1);
		p.setRemoveAbandonedTimeout(60); // 60 sec -- the longest allowed query
		p.setAbandonWhenPercentageFull(50); // only abandon if 50% of connections are in use
		p.setTestOnBorrow(true);
		p.setValidationQuery("SELECT 1 FROM DUAL");
		p.setValidationInterval(60 * 1000); // 60 seconds
		p.setJdbcInterceptors("" +
				"org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
				"org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer;"  +
				"org.apache.tomcat.jdbc.pool.interceptor.StatementCache");
		DataSource datasource = new DataSource();
		datasource.setPoolProperties(p);
		return datasource;
	}

}

