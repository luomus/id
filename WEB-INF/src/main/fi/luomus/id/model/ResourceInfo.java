package fi.luomus.id.model;

import java.util.Optional;

import fi.luomus.commons.containers.rdf.Qname;

public class ResourceInfo {

	private static final Qname MY_GATHERING = new Qname("MY.gathering");

	private final Qname id;
	private final Qname type;
	private final Optional<Qname> parentId;

	public ResourceInfo(Qname id, Qname type) {
		this(id, type, null);
	}

	public ResourceInfo(Qname id, Qname type, Qname parentId) {
		this.id = id;
		this.type = type;
		this.parentId = Optional.ofNullable(parentId);
	}

	public Qname getId() {
		return id;
	}

	public Qname getType() {
		return type;
	}

	public Optional<Qname> getParentId() {
		return parentId;
	}

	@Override
	public String toString() {
		return "ResourceInfo [uri=" + id.toURI() + ", class=" + type + ", parentUri=" + (parentId.isPresent() ? parentId.get().toURI() : null) + "]";
	}

	public boolean isGathering() {
		return MY_GATHERING.equals(getType());
	}

}
