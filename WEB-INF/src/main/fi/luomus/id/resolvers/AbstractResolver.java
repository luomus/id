package fi.luomus.id.resolvers;

import java.util.Collections;
import java.util.Set;

public abstract class AbstractResolver implements ResolveService {

	private final Set<String> prefixes;

	public AbstractResolver(Set<String> prefixes) {
		this.prefixes = Collections.unmodifiableSet(prefixes);
	}

	@Override
	public boolean isOptimizedForQnamePrefix(String prefix) {
		return prefixes.contains(prefix);
	}

	@Override
	public void close() {}

}
