package fi.luomus.id.resolvers;

import java.util.Optional;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class DwResolver extends AbstractResolver {

	private final Config config;
	private final HttpClientService httpClient;

	public DwResolver(Config config) {
		super(Utils.set());
		this.config = config;
		this.httpClient = new HttpClientService();
	}

	public void close() {
		if (httpClient != null) httpClient.close();
	}

	@Override
	public Optional<ResourceInfo> resolve(Qname id) throws Exception {
		Qname documentId = documentIdInFinBIF(id, "documentId", "/document/aggregate");
		if (documentId != null) return Optional.of(new ResourceInfo(id, new Qname("MY.document")));

		documentId = documentIdInFinBIF(id, "unitId", "/unit/aggregate");
		if (documentId != null) return Optional.of(new ResourceInfo(id, new Qname("MY.unit"), documentId));

		documentId = documentIdInFinBIF(id, "sampleId", "/sample/aggregate");
		if (documentId != null) return Optional.of(new ResourceInfo(id, new Qname("MF.sample"), documentId));

		documentId = documentIdInFinBIF(id, "gatheringId", "/gathering/aggregate");
		if (documentId != null) return Optional.of(new ResourceInfo(id, new Qname("MY.gathering"), documentId));

		return Optional.empty();
	}

	private Qname documentIdInFinBIF(Qname id, String filter, String path) throws Exception {
		String uri = id.toURI();
		URIBuilder requestUri =
				new URIBuilder(config.get("LajiApiURL")+"warehouse/query"+path)
				.addParameter("aggregateBy", "document.documentId")
				.addParameter(filter, uri)
				.addParameter("access_token", config.get("LajiApiKey"));
		HttpGet get = new HttpGet(requestUri.getURI());
		JSONObject response = getHttpClient().contentAsJson(get);
		return parseFinBifResponseDocumentId(response);
	}

	private Qname parseFinBifResponseDocumentId(JSONObject response) {
		for (JSONObject result : response.getArray("results").iterateAsObject()) {
			String documentId = result.getObject("aggregateBy").getString("document.documentId");
			if (given(documentId)) return Qname.fromURI(documentId);
		}
		return null;
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private HttpClientService getHttpClient() {
		return httpClient;
	}

}
