package fi.luomus.id.resolvers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class LajistoreResolver extends AbstractResolver {

	private static final Map<String, String> TYPES;
	static {
		TYPES = new HashMap<>();
		// annotation -- do not reveal
		TYPES.put("device", "MXC.device");
		TYPES.put("deviceIndividual", "MXC.deviceIndividual");
		// document -- resolved via dw
		TYPES.put("form", "MHL.form");
		// formPermissionSingle -- do not reveal
		TYPES.put("individual", "MXA.");
		TYPES.put("namedPlace", "MNP.namedPlace");
		TYPES.put("news", "MPO.news");
		TYPES.put("dataset", "GX.dataset");
		// notification -- do not reveal
		// profile -- do not reveal
	}

	private final DataSource oracleDataSource;
	private final String schema;


	public LajistoreResolver(DataSource oracleDataSource, Config config) {
		super(Utils.set("MHL", "MNP", "GX"));
		this.oracleDataSource = oracleDataSource;
		this.schema = config.get("LajistoreSchema");
	}

	@Override
	public Optional<ResourceInfo> resolve(Qname id) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = new SimpleTransactionConnection(oracleDataSource.getConnection());
			p = con.prepareStatement(" SELECT type FROM " +schema+ ".laji_document WHERE id = ? ");
			p.setString(1, id.toString());
			rs = p.executeQuery();
			if (!rs.next()) return Optional.empty();
			String type = TYPES.get(rs.getString(1));
			if (type == null) return Optional.empty();
			return Optional.of(new ResourceInfo(id, new Qname(type), null));
		} finally {
			Utils.close(p, rs, con);
		}
	}

}
