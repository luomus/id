package fi.luomus.id.resolvers;

import java.util.Optional;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.id.model.ResourceInfo;

public interface ResolveService {

	public Optional<ResourceInfo> resolve(Qname id) throws Exception;

	public boolean isOptimizedForQnamePrefix(String prefix);

	public void close();

}