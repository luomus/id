package fi.luomus.id.resolvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;

public class Resolvers {

	private final Config config;
	private final DataSource oracleDataSource;

	public Resolvers(Config config, DataSource oracleDataSource) {
		this.config = config;
		this.oracleDataSource = oracleDataSource;
	}

	public List<ResolveService> getServices() {
		// In optimized order from most commonly requested to less commonly requested of those namespaces that ARE NOT AMONG KNOWN optimaxed namespaces
		// For example taxon (MX.) are most commonly requested, but MX is optimized to go to triplestoreresolver
		// Here we define the order of resolvers for uknown namespaces
		List<ResolveService> services = new ArrayList<>();
		services.add(new DwResolver(config)); // specimen / document ids are most commonly requested of the undefined namespaces
		services.add(new TriplestoreResolver(oracleDataSource, config)); // anything else mostly comes from triplestore
		services.add(new LajistoreResolver(oracleDataSource, config)); // lajistore entities are very rarely requested
		return Collections.unmodifiableList(services);
	}

}
