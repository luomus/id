package fi.luomus.id.resolvers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

import org.apache.tomcat.jdbc.pool.DataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class TriplestoreResolver extends AbstractResolver {

	private final DataSource oracleDataSource;
	private final String schema;

	public TriplestoreResolver(DataSource oracleDataSource, Config config) {
		super(Utils.set("MA", "MX", "MR", "HBF", "MM", "HR", "ML", "KE", "MP", "MVL"));
		this.oracleDataSource = oracleDataSource;
		this.schema = config.get("TriplestoreSchema");
	}

	@Override
	public Optional<ResourceInfo> resolve(Qname id) throws Exception {
		TransactionConnection con = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con = new SimpleTransactionConnection(oracleDataSource.getConnection());
			p = con.prepareStatement(" SELECT objectname FROM " + schema + ".rdf_statementview WHERE subjectname = ? AND predicatename = 'rdf:type' ");
			p.setString(1, id.toString());
			rs = p.executeQuery();
			if (rs.next()) {
				String type = rs.getString(1);
				if (type != null) {
					return Optional.of(new ResourceInfo(id, new Qname(type), null));
				}
			}
			Utils.close(p, rs);
			p = con.prepareStatement(" SELECT count(1) FROM " + schema + ".rdf_statementview WHERE subjectname = ? ");
			p.setString(1, id.toString());
			rs = p.executeQuery();
			rs.next();
			if (rs.getInt(1) > 0) {
				return Optional.of(new ResourceInfo(id, new Qname("rdfs:Resource"), null));
			}
			return Optional.empty();
		} finally {
			Utils.close(p, rs, con);
		}
	}

}
