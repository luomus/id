package fi.luomus.id.viewers;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.riot.RDFFormat;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public abstract class AbstractModelViewer implements ViewerMapping {

	protected final DAO dao;
	protected final ErrorReporter errorReporter;

	public AbstractModelViewer(DAO dao, ErrorReporter errorReporter) {
		this.dao = dao;
		this.errorReporter = errorReporter;
	}
	
	protected abstract Model getModel(ResourceInfo info) throws Exception;

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return viewHtml(info);
		}
		if (getSupportedFormats().contains(format)) {
			return viewSerialized(info, format);
		}
		throw new UnsupportedFormatException(format);
	}

	public ResponseData viewHtml(ResourceInfo info) throws Exception {
		return viewHtml(info, null);
	}

	public ResponseData viewHtml(ResourceInfo info, Set<String> whitelist) throws Exception {
		List<Statement> statements = getStatements(info, whitelist);
		return new ResponseData()
				.setData("id", info.getId().toURI())
				.setData("type", info.getType())
				.setData("statements", statements)
				.setViewName("triplestore");
	}

	private List<Statement> getStatements(ResourceInfo info, Set<String> whitelist) throws Exception {
		Model model = getModel(info);
		List<Statement> statements = getStatements(model, whitelist);
		return statements;
	}

	private List<Statement> getStatements(Model model, Set<String> whitelist) {
		List<Statement> statements = new ArrayList<>();
		for (Statement s : model.getStatements()) {
			if (whitelist == null || whitelist.contains(s.getPredicate().getQname())) {
				statements.add(s);
			}
			if (shouldHide(s)) return Collections.emptyList();
		}
		Collections.sort(statements, new Comparator<Statement>() {
			@Override
			public int compare(Statement o1, Statement o2) {
				return o1.getPredicate().getQname().compareTo(o2.getPredicate().getQname());
			}
		});
		return statements;
	}

	private boolean shouldHide(Statement s) {
		if (s.getPredicate().getQname().equals("MZ.publicityRestrictions")) {
			if (s.isResourceStatement()) {
				if (!s.getObjectResource().getQname().equals("MZ.publicityRestrictionsPublic")) {
					return true;
				}
			}
		}
		return false;
	}

	public ResponseData viewSerialized(ResourceInfo info, Format format) throws Exception {
		return viewSerialized(info, format, null);
	}

	public ResponseData viewSerialized(ResourceInfo info, Format format, Set<String> whitelist) throws Exception {
		return viewSerialized(info, format, toRdfFormat(format), whitelist);
	}

	private RDFFormat toRdfFormat(Format format) throws UnsupportedFormatException {
		if (format == Format.RDFXML || format == Format.XML) return RDFFormat.RDFXML_ABBREV;
		if (format == Format.JSONLD || format == Format.JSON) return RDFFormat.JSONLD;
		if (format == Format.TRIG) return RDFFormat.TRIG;
		throw new UnsupportedFormatException(format);
	}

	private ResponseData viewSerialized(ResourceInfo info, Format format, RDFFormat rdfFormat, Set<String> whitelist) throws Exception {
		List<Statement> statements = getStatements(info, whitelist);
		String serialized = null;
		try {
			serialized = tryToSerialize(info, rdfFormat, statements);
		} catch (Exception e) {
			errorReporter.report(e);
			serialized = tryToSerializeWithoutFailingStatements(info, rdfFormat, statements);
		}
		return new ResponseData(serialized, Util.contentType(format));
	}

	private String tryToSerializeWithoutFailingStatements(ResourceInfo info, RDFFormat rdfFormat, List<Statement> statements) {
		List<Statement> workingStatements = new ArrayList<>();
		for (Statement s : statements) {
			try {
				tryToSerialize(info, rdfFormat, Utils.list(s));
				workingStatements.add(s);
			} catch (Exception e) {
				// don't include this statement
			}
		}
		return tryToSerialize(info, rdfFormat, workingStatements);
	}

	private String tryToSerialize(ResourceInfo info, RDFFormat rdfFormat, List<Statement> statements) {
		Model model = new Model(info.getId());
		model.setType(info.getType().toString());
		for (Statement s : statements) {
			model.addStatement(s);
		}
		String serialized = model.getSerialized(rdfFormat);
		return serialized;
	}

}
