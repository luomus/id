package fi.luomus.id.viewers;

import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.viewers.ViewerMapping.Format;

public class ApiViewer {

	private final DAO dao;

	public ApiViewer(DAO dao) {
		this.dao = dao;
	}

	public ResponseData view(Qname id, String endpoint, Map<String, String> localeParameters, Format format) throws Exception {
		return new ResponseData(dao.getApiResponse(endpoint, id, localeParameters), Util.contentType(format));
	}

	public ResponseData view(Qname id, String endpoint, Map<String, String> localeParameters, Map<String, String> additionalParams, Format format) throws Exception {
		return new ResponseData(dao.getApiResponse(endpoint, id, localeParameters, additionalParams), Util.contentType(format));
	}

}
