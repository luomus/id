package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class ChecklistViewer implements ViewerMapping {

	private final ApiViewer api;
	private final TriplestoreViewer triplestore;
	private final DAO dao;

	public ChecklistViewer(TriplestoreViewer triplestore, ApiViewer api, DAO dao) {
		this.api = api;
		this.triplestore = triplestore;
		this.dao = dao;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.SKOS, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception, UnsupportedFormatException {
		if (format == Format.JSON || format == Format.JSONLD) {
			return api.view(info.getId(), "checklists", localeParameters, format);
		}
		if (format == Format.XML || format == Format.RDFXML || format == Format.TRIG) {
			return triplestore.viewSerialized(info, format);
		}
		if (format == Format.HTML) {
			return triplestore.viewHtml(info);
		}
		if (format == Format.SKOS) {
			return skos(info);
		}
		throw new UnsupportedFormatException(format);
	}

	private ResponseData skos(ResourceInfo info) throws Exception {
		Model ourModel = dao.getTriplestoreModel(info.getId());
		Model skosModel = new Model(info.getId());
		skosModel.setType("skos:ConceptScheme");
		for (Statement s : ourModel.getStatements("dc:bibliographicCitation")) {
			skosModel.addStatement(new Statement(new Predicate("rdfs:label"), s.getObjectLiteral()));
		}
		for (Statement s : ourModel.getStatements("MR.rootTaxon")) {
			skosModel.addStatement(new Statement(new Predicate("skos:hasTopConcept") , s.getObjectResource()));
		}
		skosModel.addStatement(new Statement(new Predicate("so:license"), new ObjectResource("cc:licenses/by/4.0/")));
		String skos = skosModel.getRDF();
		return new ResponseData(skos, Util.contentType(Format.SKOS));
	}

}
