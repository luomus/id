package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class InformalTaxonGroupViewer implements ViewerMapping {

	private final ApiViewer api;
	private final TriplestoreViewer triplestore;

	public InformalTaxonGroupViewer(TriplestoreViewer triplestore, ApiViewer api) {
		this.api = api;
		this.triplestore = triplestore;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (format == Format.HTML) {
			return Util.redirect("https://laji.fi/in/taxon/browse?informalGroupFilters="+info.getId().toString(), localeParameters);
		}
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception, UnsupportedFormatException {
		if (format == Format.JSON || format == Format.JSONLD) {
			return api.view(info.getId(), "informal-taxon-groups", localeParameters, format);
		}
		if (format == Format.XML || format == Format.RDFXML || format == Format.TRIG) {
			return triplestore.viewSerialized(info, format);
		}
		if (format == Format.HTML) {
			return new ResponseData().setRedirectLocation(getRedirectURI(info, format, localeParameters).toString());
		}
		throw new UnsupportedFormatException(format);
	}

}
