package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class InformationSystemViewer implements ViewerMapping {

	private static final Set<String> WHITELIST = Utils.set(
			"KE.description",
			"KE.maintainingOrganization",
			"KE.name",
			"KE.type");

	private final ApiViewer api;
	private final TriplestoreViewer triplestore;

	public InformationSystemViewer(TriplestoreViewer triplestore, ApiViewer api) {
		this.api = api;
		this.triplestore = triplestore;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception, UnsupportedFormatException {
		if (format == Format.JSON || format == Format.JSONLD) {
			return api.view(info.getId(), "sources", localeParameters, format);
		}
		if (format == Format.XML || format == Format.RDFXML || format == Format.TRIG) {
			return triplestore.viewSerialized(info, format, WHITELIST);
		}
		if (format == Format.HTML) {
			return triplestore.viewHtml(info, WHITELIST);
		}
		throw new UnsupportedFormatException(format);
	}

}
