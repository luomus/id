package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class LajiFormViewer implements ViewerMapping {

	private DAO dao;
	private ApiViewer api;

	public LajiFormViewer(DAO dao, ApiViewer api) {
		this.dao = dao;
		this.api = api;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.JSON) {
			return api.view(info.getId(), "forms", localeParameters, format);
		}
		JSONObject json = new JSONObject(dao.getApiResponse("forms", info.getId(), localeParameters));
		if (format == Format.HTML) {
			return new ResponseData()
					.setData("id", info.getId().toURI())
					.setData("type", info.getType())
					.setViewName("plain").setData("data", Utils.toHTMLEntities(json.beautify()));
		}
		throw new UnsupportedFormatException(format);
	}

}
