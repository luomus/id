package fi.luomus.id.viewers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class LajistoreViewer extends AbstractModelViewer {

	public LajistoreViewer(DAO dao, ErrorReporter errorReporter) {
		super(dao, errorReporter);
	}

	@Override
	protected Model getModel(ResourceInfo info) throws Exception {
		JSONObject json = dao.getLajistoreModel(info.getId());
		Model model = new Model(info.getId());
		if (json == null) return model;
		Map<String, Property> context = getContext(json);
		for (String key : json.getKeys()) {
			if (key.startsWith("@")) continue;
			Property property = context.get(key);
			if (json.isObject(key)) {
				for (Statement s : getLocalizedStatements(property, json.getObject(key))) {
					model.addStatement(s);
				}
			} else if (json.isArray(key)) {
				for (Statement s : getArrayStatements(property, json.getArray(key))) {
					model.addStatement(s);
				}
			} else {
				Statement s = getStatement(property, json.getString(key));
				if (s != null) model.addStatement(s);
			}
		}
		return model;
	}

	private static final Map<String, Map<String, Property>> CONTEXT_CACHE = new HashMap<>();

	private Map<String, Property> getContext(JSONObject json) {
		String context = json.getString("@context");
		if (!given(context)) return Collections.emptyMap();
		if (!CONTEXT_CACHE.containsKey(context)) {
			Map<String, Property> map = getContext(context);
			if (!map.isEmpty()) {
				CONTEXT_CACHE.put(context, map);
			}
			return map;
		}
		return CONTEXT_CACHE.get(context);
	}

	private static class Property {
		private final Predicate predicate;
		private final boolean isResource;
		public Property(JSONObject json) {
			String property = json.getString("@id");
			if (property.startsWith("default:")) property = property.replace("default:", "");
			this.predicate = new Predicate(property);
			this.isResource =  "@id".equals(json.getString("@type"));
		}
	}

	private Map<String, Property> getContext(String context) {
		HttpClientService client = null;
		try {
			client = new HttpClientService();
			JSONObject response = client.contentAsJson(new HttpGet(context)).getObject("@context");
			Map<String, Property> properties = new HashMap<>();
			for (String key : response.getKeys()) {
				if (key.startsWith("@")) continue;
				if (response.isObject(key)) {
					properties.put(key, new Property(response.getObject(key)));
				}
			}
			return properties;
		} catch (Exception e) {
			errorReporter.report("Failed to get context " + context, e);
			return Collections.emptyMap();
		} finally {
			if (client != null) client.close();
		}
	}

	private Statement getStatement(Property property, String value) {
		if (property == null || !given(value)) return null;
		if (property.isResource) return new Statement(property.predicate, new ObjectResource(new Qname(value)));
		return new Statement(property.predicate, new ObjectLiteral(value));
	}

	private Collection<Statement> getArrayStatements(Property property, JSONArray array) {
		if (property == null || array.isEmpty()) return Collections.emptyList();
		List<Statement> s = new ArrayList<>();
		for (String value : array.iterateAsString()) {
			Statement stmt = getStatement(property, value);
			if (stmt != null) s.add(stmt);
		}
		return s;
	}

	private Collection<Statement> getLocalizedStatements(Property property, JSONObject object) {
		if (property == null || object.isEmpty()) return Collections.emptyList();
		List<Statement> s = new ArrayList<>();
		for (String locale : object.getKeys()) {
			s.add(new Statement(property.predicate, new ObjectLiteral(object.getString(locale), locale)));
		}
		return s;
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

}
