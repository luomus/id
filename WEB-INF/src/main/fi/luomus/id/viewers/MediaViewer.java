package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class MediaViewer implements ViewerMapping {

	private final TriplestoreViewer triplestore;

	public MediaViewer(TriplestoreViewer triplestore) {
		this.triplestore = triplestore;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	private static final Set<String> WHITELIST = Utils.set(
			"MM.originalURL",
			"MM.fullURL",
			"MM.largeURL",
			"MM.squareThumbnailURL",
			"MM.thumbnailURL",
			"MM.mp3URL",
			"MM.wavURL",
			"MM.videoURL",
			"MM.highDetailModelURL",
			"MM.lowDetailModelURL",
			"MM.taxonURI",
			"MM.taxonVerbatim",
			"MM.taxonDescriptionCaption",
			"MZ.intellectualRights",
			"MZ.intellectualOwner",
			"MM.keyword",
			"MZ.publicityRestrictions",
			"MM.sourceSystem",
			"MM.fullResolutionMediaAvailable",
			"MY.lifeStage",
			"MY.plantLifeStage",
			"MM.primaryForTaxon",
			"MY.sex",
			"MM.type");

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return triplestore.viewHtml(info, WHITELIST);
		}
		return triplestore.viewSerialized(info, format, WHITELIST);
	}

}
