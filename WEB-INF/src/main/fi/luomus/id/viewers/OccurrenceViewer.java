package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;

import fi.luomus.commons.containers.rdf.JenaUtils;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class OccurrenceViewer implements ViewerMapping {

	private static final String FINBIF = "Finnish Biodiversity Information Facility";
	private static final String RDF_RESOURCE = "rdf:resource";
	private final DAO dao;

	public OccurrenceViewer(DAO dao) {
		this.dao = dao;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (format == Format.HTML) {
			URIBuilder b = new URIBuilder("https://laji.fi/in/view");
			if (info.getParentId().isPresent()) {
				b.addParameter("uri", info.getParentId().get().toURI());
				if (!info.isGathering()) {
					b.addParameter("highlight", info.getId().toURI());
				}
			} else {
				b.addParameter("uri", info.getId().toURI());
			}
			Util.addLocaleParams(b, localeParameters);
			return Util.redirect(b.toString());
		}
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		Qname documentId = info.getParentId().isPresent() ? info.getParentId().get() : info.getId();
		if (format == Format.JSON || format == Format.XML) {
			String data = dao.getDwResponse("document", documentId, Util.contentType(format));
			return new ResponseData(data, Util.contentType(format));
		}
		if (format == Format.RDFXML || format == Format.JSONLD || format == Format.TRIG) {
			return cetaf(info, format, documentId);
		}
		if (format == Format.HTML) {
			return new ResponseData().setRedirectLocation(getRedirectURI(info, format, localeParameters).toString());
		}
		throw new UnsupportedFormatException(format);
	}

	private ResponseData cetaf(ResourceInfo info, Format format, Qname documentId) throws Exception {
		String rdfxml = getAndComplementCetafRDFXML(info, documentId);
		if (format == Format.RDFXML) {
			return new ResponseData(rdfxml, Util.contentType(format));
		}
		if (format == Format.JSONLD) {
			return convertTo(RDFFormat.JSONLD, rdfxml, format);
		}
		if (format == Format.TRIG) {
			return convertTo(RDFFormat.TRIG, rdfxml, format);
		}
		throw new IllegalArgumentException(format.name());
	}

	private ResponseData convertTo(RDFFormat targetFormat, String rdfxml, Format format) {
		Model jenaModel = JenaUtils.read(rdfxml);
		String serialized = JenaUtils.getSerialized(jenaModel, targetFormat);
		return new ResponseData(serialized, Util.contentType(format));
	}

	private String getAndComplementCetafRDFXML(ResourceInfo info, Qname documentId) throws Exception {
		String rdfxml = dao.getDwResponse("unit/list", documentId, Util.contentType(Format.RDFXML));

		Document doc = new XMLReader().parse(rdfxml);
		Node root = doc.getRootNode();
		addRDFInfo(root, info, documentId);
		addHTMLInfo(root, info, documentId);
		addJSONLDInfo(root, info, documentId);
		addJSONInfo(root, info, documentId);

		rdfxml = new XMLWriter(doc).generateXML();
		return rdfxml;
	}

	private void addJSONInfo(Node root, ResourceInfo info, Qname documentId) {
		addInfo(root, documentId, getRedirectURI(info, Format.JSON), Format.JSON);
	}

	private void addJSONLDInfo(Node root, ResourceInfo info, Qname documentId) {
		addInfo(root, documentId, getRedirectURI(info, Format.JSONLD), Format.JSONLD);
	}

	private void addHTMLInfo(Node root, ResourceInfo info, Qname documentId) {
		addInfo(root, documentId, getRedirectURI(info, Format.HTML), Format.HTML);
	}

	private void addRDFInfo(Node root, ResourceInfo info, Qname documentId) {
		addInfo(root, documentId, getRedirectURI(info, Format.RDFXML), Format.RDFXML);
	}

	private void addInfo(Node root, Qname documentId, String url, Format format) {
		Node node = root.addChildNode("rdf:Description").addAttribute("rdf:about", url);
		node.addChildNode("rdf:type").addAttribute(RDF_RESOURCE, "http://purl.org/dc/dcmitype/Text");
		node.addChildNode("rdfschema:comment").addAttribute("xml:lang", "en").setContents("Describing the " + format + " representation of the document");
		node.addChildNode("dc:subject").addAttribute(RDF_RESOURCE, documentId.toURI());
		node.addChildNode("dc:type").addAttribute(RDF_RESOURCE, "http://purl.org/dc/dcmitype/Dataset");
		node.addChildNode("dc:created").setContents(DateUtils.getCurrentDateTime("yyyy-MM-dd"));
		node.addChildNode("dc:creator").setContents(FINBIF);
		node.addChildNode("dc:publisher").setContents(FINBIF);
		node.addChildNode("dc:format").setContents(Util.contentType(format));
	}

	private String getRedirectURI(ResourceInfo info, Format format) {
		try {
			return getRedirectURI(info, format, null).toString();
		} catch (UnsupportedFormatException e) {
			throw new IllegalStateException("Impossible state", e);
		}
	}

}
