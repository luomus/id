package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.apache.jena.riot.RDFFormat;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class OnlyTypeViewer implements ViewerMapping {

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return new ResponseData()
					.setData("id", info.getId().toURI())
					.setData("type", info.getType())
					.setViewName("triplestore");
		}
		if (format == Format.JSON) {
			JSONObject json = new JSONObject().setString("class", info.getType().toString());
			return new ResponseData(json.toString(), Util.contentType(format));
		}
		Model model = new Model(info.getId());
		model.setType(info.getType().toString());
		if (format == Format.XML || format == Format.RDFXML) {
			String xml = model.getRDF();
			return new ResponseData(xml, Util.contentType(format));
		}
		if (format == Format.JSON || format == Format.JSONLD) {
			String json = model.getSerialized(RDFFormat.JSONLD);
			return new ResponseData(json, Util.contentType(format));
		}
		if (format == Format.TRIG) {
			String trig = model.getSerialized(RDFFormat.TRIG);
			return new ResponseData(trig, Util.contentType(format));
		}
		throw new UnsupportedFormatException(format);
	}

}
