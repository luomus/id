package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class OrganizationViewer implements ViewerMapping {

	private final TriplestoreViewer triplestore;

	public OrganizationViewer(TriplestoreViewer triplestore) {
		this.triplestore = triplestore;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	private static final Set<String> WHITELIST = Utils.set(
			"MOS.abbreviation",
			"MOS.abbreviationExplanation",
			"MOS.country",
			"MOS.locality",
			"MOS.organizationLevel1",
			"MOS.organizationLevel2",
			"MOS.organizationLevel3",
			"MOS.organizationLevel4",
			"MOS.region",
			"MZ.dateCreated",
			"MZ.dateEdited");

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return triplestore.viewHtml(info, WHITELIST);
		}
		return triplestore.viewSerialized(info, format, WHITELIST);
	}

}
