package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class PersonViewer implements ViewerMapping {

	private final TriplestoreViewer triplestore;
	private final ApiViewer api;

	public PersonViewer(TriplestoreViewer triplestore, ApiViewer api) {
		this.triplestore = triplestore;
		this.api = api;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (format == Format.HTML) {
			return Util.redirect("https://laji.fi/in/user/" +info.getId().toString(), localeParameters);
		}
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	private static final Set<String> WHITELIST = Utils.set(
			"MA.fullName",
			"MA.group");

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return new ResponseData().setRedirectLocation(getRedirectURI(info, format, localeParameters).toString());
		}
		if (format == Format.JSON) {
			return api.view(info.getId(), "person/by-id", localeParameters, format);
		}
		return triplestore.viewSerialized(info, format, WHITELIST);
	}

}
