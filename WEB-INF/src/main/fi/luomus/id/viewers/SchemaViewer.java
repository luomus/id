package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.model.ResourceInfo;

public class SchemaViewer implements ViewerMapping {

	private final TriplestoreViewer triplestore;

	public SchemaViewer(TriplestoreViewer triplestore) {
		this.triplestore = triplestore;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (format == Format.HTML) {
			String path = info.getType().toString().toLowerCase().split(Pattern.quote(":"))[1];
			return Util.redirect("http://schema.laji.fi/" + path + "/" + info.getId() , localeParameters);
		}
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception {
		if (format == Format.HTML) {
			return new ResponseData().setRedirectLocation(getRedirectURI(info, format, localeParameters).toString());
		}
		return triplestore.viewSerialized(info, format);
	}

}
