package fi.luomus.id.viewers;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.Utils;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class TaxonViewer implements ViewerMapping {

	private static final Map<String, String> API_ADDITIONAL_PARAMS;
	static {
		API_ADDITIONAL_PARAMS = new HashMap<>();
		API_ADDITIONAL_PARAMS.put("includeMedia", "true");
		API_ADDITIONAL_PARAMS.put("includeDescriptions", "true");
	}

	private final ApiViewer api;
	private final TriplestoreViewer triplestore;
	private final DAO dao;

	public TaxonViewer(TriplestoreViewer triplestore, ApiViewer api, DAO dao) {
		this.api = api;
		this.triplestore = triplestore;
		this.dao = dao;
	}

	@Override
	public List<Format> getSupportedFormats() {
		return Utils.list(Format.HTML, Format.JSON, Format.JSONLD, Format.XML, Format.RDFXML, Format.SKOS, Format.TRIG);
	}

	@Override
	public URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException {
		if (format == Format.HTML) {
			return Util.redirect("https://laji.fi/in/taxon/" +info.getId().toString(), localeParameters);
		}
		if (getSupportedFormats().contains(format)) {
			return Util.self(info, format, localeParameters);
		}
		throw new UnsupportedFormatException(format);
	}

	@Override
	public ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception, UnsupportedFormatException {
		if (format == Format.JSON || format == Format.JSONLD) {
			return api.view(info.getId(), "taxa", localeParameters, API_ADDITIONAL_PARAMS, format);
		}
		if (format == Format.XML || format == Format.RDFXML || format == Format.TRIG) {
			return triplestore.viewSerialized(info, format);
		}
		if (format == Format.SKOS) {
			return skos(info);
		}
		if (format == Format.HTML) {
			return new ResponseData().setRedirectLocation(getRedirectURI(info, format, localeParameters).toString());
		}
		throw new UnsupportedFormatException(format);
	}

	private ResponseData skos(ResourceInfo info) throws Exception {
		Model ourModel = dao.getTriplestoreModel(info.getId());
		Model skosModel = new Model(info.getId());
		skosModel.setType("skos:Concept");
		for (Statement s : ourModel.getStatements("MX.nameAccordingTo")) {
			skosModel.addStatement(new Statement(new Predicate("skos:inScheme"), s.getObjectResource()));
		}
		for (Statement s : ourModel.getStatements("skos:exactMatch")) {
			skosModel.addStatement(s);
		}
		for (Statement s : ourModel.getStatements("MX.scientificName")) {
			skosModel.addStatement(new Statement(new Predicate("dwc:scientificName"), s.getObjectLiteral()));
		}
		for (Statement s : ourModel.getStatements("MX.scientificNameAuthorship")) {
			skosModel.addStatement(new Statement(new Predicate("dwc:scientificNameAuthorship"), s.getObjectLiteral()));
		}
		for (Statement s : ourModel.getStatements("MX.vernacularName")) {
			skosModel.addStatement(new Statement(new Predicate("dwc:vernacularName"), s.getObjectLiteral()));
		}
		for (Statement s : ourModel.getStatements("MX.taxonRank")) {
			skosModel.addStatement(new Statement(new Predicate("dwc:taxonRank"), new ObjectLiteral(s.getObjectResource().getQname().replace("MX.", ""))));
		}
		skosModel.addStatement(new Statement(new Predicate("so:license"), new ObjectResource("cc:licenses/by/4.0/")));
		String skos = skosModel.getRDF();
		return new ResponseData(skos, Util.contentType(Format.SKOS));
	}

}
