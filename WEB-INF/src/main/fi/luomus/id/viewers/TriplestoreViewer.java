package fi.luomus.id.viewers;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.id.dao.DAO;
import fi.luomus.id.model.ResourceInfo;

public class TriplestoreViewer extends AbstractModelViewer {

	public TriplestoreViewer(DAO dao, ErrorReporter errorReporter) {
		super(dao, errorReporter);
	}

	protected Model getModel(ResourceInfo info) throws Exception {
		return dao.getTriplestoreModel(info.getId());
	}

}
