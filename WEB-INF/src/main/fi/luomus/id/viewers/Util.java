package fi.luomus.id.viewers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.id.IdService;
import fi.luomus.id.model.ResourceInfo;
import fi.luomus.id.viewers.ViewerMapping.Format;

public class Util {

	public static URI redirect(String uri) {
		try {
			return new URI(uri);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public static URI redirect(String baseURI, Map<String, String> localeParameters) {
		URIBuilder b = new URIBuilder(baseURI.replace("http:", "https:"));
		addLocaleParams(b, localeParameters);
		return redirect(b.toString());
	}

	public static URI self(ResourceInfo info, Format format, Map<String, String> localeParameters) {
		String uri = info.getId().toURI();
		if (info.getId().getNamespace().equals("utu")) {
			uri = "https://tun.fi/" + info.getId(); // mus.utu.fi redirect does not include parameters so redirect directly to self using qname
		}
		URIBuilder b = new URIBuilder(uri.replace("http:", "https:"));
		b.addParameter("format", format.name());
		addLocaleParams(b, localeParameters);
		return redirect(b.toString());
	}

	public static void addLocaleParams(URIBuilder b, Map<String, String> localeParameters) {
		if (localeParameters == null) return;
		for (Map.Entry<String, String> e : localeParameters.entrySet()) {
			b.addParameter(e.getKey(), e.getValue());
		}
	}

	public static String contentType(Format format) {
		String contentType = IdService.FORMAT_TO_ACCEPT_HEADER.get(format);
		if (contentType != null) return contentType;
		throw new UnsupportedOperationException("Not yet implemented " + format);
	}

}
