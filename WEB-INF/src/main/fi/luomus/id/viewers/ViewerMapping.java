package fi.luomus.id.viewers;

import java.net.URI;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.id.model.ResourceInfo;

public interface ViewerMapping {

	public static class UnsupportedFormatException extends Exception {
		private static final long serialVersionUID = -1481414399253740131L;
		public UnsupportedFormatException(Format format) {
			super(format.name());
		}
		public UnsupportedFormatException(String format) {
			super(format);
		}
	}

	public enum Format { HTML, JSON, JSONLD, XML, RDFXML, SKOS, TRIG }

	List<Format> getSupportedFormats();

	URI getRedirectURI(ResourceInfo info, Format format, Map<String, String> localeParameters) throws UnsupportedFormatException;

	ResponseData view(ResourceInfo info, Format format, Map<String, String> localeParameters) throws Exception, UnsupportedFormatException;

}
