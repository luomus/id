package fi.luomus.id.viewers;

import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.id.dao.DAO;

public class ViewerMappings {

	private final OccurrenceViewer occurrence;
	private final TriplestoreViewer triplestore;
	private final LajistoreViewer lajistore;
	private final SchemaViewer schema;
	private final OnlyTypeViewer onlyType;
	private final ApiViewer api;
	private final Map<String, ViewerMapping> map;
	private final DAO dao;

	public ViewerMappings(DAO dao, ErrorReporter errorReporter) {
		this.dao = dao;
		occurrence = new OccurrenceViewer(dao);
		triplestore = new TriplestoreViewer(dao, errorReporter);
		api = new ApiViewer(dao);
		lajistore = new LajistoreViewer(dao, errorReporter);
		schema = new SchemaViewer(triplestore);
		onlyType = new OnlyTypeViewer();
		map = new HashMap<>();
		initMappings();
	}

	private void initMappings() {
		map.put("MY.document", occurrence);
		map.put("MY.gathering", occurrence);
		map.put("MY.unit", occurrence);
		map.put("MF.sample", occurrence);
		map.put("MY.identification", triplestore);
		map.put("MY.typeSpecimen", triplestore);
		map.put("PUU.branch", triplestore);

		map.put("MA.person", new PersonViewer(triplestore, api));
		map.put("MX.taxon", new TaxonViewer(triplestore, api, dao));
		map.put("MR.checklist", new ChecklistViewer(triplestore, api, dao));
		map.put("HBF.downloadRequest", new DownloadCitationViewer(triplestore));
		map.put("MY.collection", new CollectionMetadataViewer(triplestore, api));
		map.put("MVL.informalTaxonGroup", new InformalTaxonGroupViewer(triplestore, api));
		map.put("MOS.organization", new OrganizationViewer(triplestore));
		map.put("KE.informationSystem", new InformationSystemViewer(triplestore, api));
		map.put("MM.image", new MediaViewer(triplestore));
		map.put("MM.audio", new MediaViewer(triplestore));
		map.put("MM.video", new MediaViewer(triplestore));
		map.put("MHL.form", new LajiFormViewer(dao, api));

		map.put("rdf:Property", schema);
		map.put("rdfs:Class", schema);
		map.put("rdf:Alt", schema);

		map.put("rdfs:Resource", triplestore);
		map.put("MR.checklistVersion", triplestore);
		map.put("MP.publication", triplestore);
		map.put("ML.area", triplestore);
		map.put("MVL.iucnRedListTaxonGroup", triplestore);
		map.put("MO.occurrence", triplestore);
		map.put("MKV.habitatObject", triplestore);
		map.put("MKV.endangermentObject", triplestore);
		map.put("MI.taxonInteraction", triplestore);
		map.put("LA.Pinkka", triplestore);
		map.put("MMAN.tagClass", triplestore);
		map.put("MX.administrativeStatus", triplestore);
		map.put("HBE.invasiveSpeciesMainGroup", triplestore);

		map.put("GX.dataset", lajistore);
	}

	public ViewerMapping forType(Qname className) {
		ViewerMapping viewer = map.get(className.toString());
		if (viewer != null) return viewer;
		return onlyType;
	}

}
