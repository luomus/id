CREATE TABLE resolved ( 
uri						VARCHAR2(2000)		NOT NULL PRIMARY KEY,
type					VARCHAR2(100)		NOT NULL,
parent_resource_uri		VARCHAR2(2000),
req_count				NUMBER(33)			DEFAULT 1 NOT NULL
);

CREATE INDEX resolved_ix_req_count ON resolved(req_count DESC);

GRANT SELECT ON <LAJISTORE_SCHEMA>.laji_document TO <user>;
GRANT SELECT ON <TRIPLESTORE_SCHEMA>.rdf_statementview TO <user>;
