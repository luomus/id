FinBIF URI redirect service
======================================

## http://tun.fi/{Qname}

Provides redirect service for FinBIF URI Identifiers

### Parameters

accepts-header (default is text/html) for redirection  

* text/html
* application/xml 
* application/rdf+xml 
* application/json
* application/ld+json
* application/skos+rdf+xml 

or format parameter for showing a view

* HTML, XML, RDFXML, JSON, JSONLD, SKOS

### Returns

* 200 - if format parameter was given 
* 302 - if format parameter was not given redirected to URI that provides information in the requested format
* 400 - if no valid ID given
* 404 - resource is not known
* 406 - unsupported accept header/format - body message tells supported types for this type of resource
* 5xx - in case of system errors

## /class?uri={uri}[&uri={uri}&uri={uri}&...]&qname={qname}[&qname={qname}&...]

Can be used to resolve class of FinBIF URI Identifier(s)

### Parameters

* uri: the identifier - you can give this parameter multiple times
* qname: identifier Qname - you can give this parameter multiple times 

### Returns

* 200 - normally, see response bellow
* 400 - for invalid parameters
* 5xx - in case of system errors

~~~
{
	"http://tun.fi/MX.123": { class: "MX.taxon" },
	"MY.123": { class: "MY.document"  },
	"FOO.123": { }, -- not known
	"http://tun.fi/JX.123#5": {	class: "MY.unit", parent: "JX.123", parentUri: "http://tun.fi/JX.123" },
	...
}
~~~

## Installation

1. See /documentation/create_tables.sql
2. Place config file to <catalina.base>/app-con/id-redirect-service.properties

Config file example

~~~
SystemID = id
DevelopmentMode = YES
StagingMode = NO
ProductionMode= NO

BaseURL = https://tun.fi
StaticURL = https://tun.fi/static

BaseFolder = ...tomcat/webapps/
TemplateFolder = id/template
LanguageFileFolder = id/locale
LanguageFilePrefix =
SupportedLanguages =

ErrorReporting_SMTP_Host = localhost
ErrorReporting_SMTP_Username =
ErrorReporting_SMTP_Password =
ErrorReporting_SMTP_SendTo = ...
ErrorReporting_SMTP_SendFrom = ...
ErrorReporting_SMTP_Subject = ID Redirect Service - Error Report

DBdriver = oracle.jdbc.OracleDriver
DBurl = jdbc:oracle:thin:@//....:1521/...
DBusername = ...
DBpassword = ...

TriplestoreURL = https://...
TriplestoreUsername = ...
TriplestorePassword = ...

LajiApiURL = https://...
LajiApiKey = ..

LajistoreSchema = 
TriplestoreSchema = 
~~~