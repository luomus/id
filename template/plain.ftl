<!doctype html>

<html lang="en">

<head>
  
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <title>${id}</title>
	<link href="${staticURL}/favicon.ico?15774543771" type="image/ico" rel="shortcut icon" />
    <link href="${staticURL}/bootstrap.min.css" rel="stylesheet">
    <link href="${staticURL}/lajifi_style.css?1577454377" rel="stylesheet">
	<link href="${staticURL}/public.css?1577454377" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand uppercase" href="https://laji.fi">FinBIF laji.fi</a>
        </div>
    </div>
</nav>
 
<div class="container">
	<h1>${id}</h1>
	<h3><a href="${type.toURI()}">${type}</a></h3>
	<br />
	<pre>${data}</pre>
</div>

</body>
</html>